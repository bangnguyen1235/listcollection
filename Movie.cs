﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListCollection
{
    public class Movie : IComparable<Movie>
    {
        private int _id;

        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _director;

        public string Director
        {
            get { return _director; }
            set { _director = value; }
        }
        public void ShowMovie() { 
        
        }  

        public int CompareTo(Movie movie)
        {
            if (this.ID > movie.ID) {
                return 1;
            }            
            else if (this.ID < movie.ID) return -1;
            else return 0;
        }
    
    }
}
