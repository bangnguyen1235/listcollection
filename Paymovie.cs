﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListCollection
{
    internal class Paymovie : IEnumerable
    {
        private readonly List<Movie> _list = null;
        public Paymovie(List<Movie> movies) { 
            _list = movies;
        }   

        public IEnumerator GetEnumerator()
        {
            return _list.GetEnumerator();   
        }
        
    }
}
