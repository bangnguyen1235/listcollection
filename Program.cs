﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListCollection
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<Movie> movies = new List<Movie>(); 
            movies.Add(new Movie { ID  = 234 ,Name =  "Doctor Strange 2",Director = "Peter White"});
            movies.Add(new Movie { ID = 45, Name = "No way home 5", Director = "Peter White" });
            movies.Add(new Movie { ID = 564, Name = "Avanger new thumder 2", Director = "Peter White" });
            movies.Add(new Movie { ID = 849, Name = "Spider man 2", Director = "Peter White" });
            movies.Add(new Movie { ID = 849, Name = "Movie4", Director = "Peter White" });

            // xoa bat ki phan tu nao trong list
            movies.RemoveAt(3);

            //chen mot phan tu bat ki vao vi tri thu 2       
            movies.Insert(2, new Movie { ID = 12433, Name = "No name", Director = "NO" });

            // sap xep cac phan thu theo thu tu giam dan theo ID
            movies.Sort();

            //tim phan tu co name la "Movie4"            

            foreach (var item in movies) {
                if (item.Name.Equals("Movie4")) {
                    Console.WriteLine(item.Name + "\n" + item.ID + "\n" + item.Director );
                }
            }
            
            Console.ReadLine();

        }
    }
}
